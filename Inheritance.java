import java.util.*;
class Object{
    Object(){
        System.out.println("Base created");
    }
}

class Vehicle extends Object{ //Single inheritance
    protected String type="Vehicle";
    Vehicle(){
        System.out.println("A Vechicle");
    }
}

class Car extends Vehicle{ //Multilevel inheritance
    Car(){
        type="Car";
        System.out.println("A car");
    }
}

class Truck extends Vehicle { //Hierachical inheritance
    Truck(){
        type="Truck";
        System.out.println("A truck");
    }
}

class Inheritance{
    public static void main(String args[]){
        Car c=new Car();
        Truck t=new Truck();
    }
}
